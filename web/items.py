import re
from scrapy import Item, Field
from scrapy.loader.processors import MapCompose, TakeFirst, Identity
from typing import Any, Optional, List, Dict


def strip_string(x: Any) -> Optional[str]:
    """
    String preprocessing.
    """
    return str(x).strip() if x is not None else x

def filter_price(raw_price: str) -> float:
    """
    Price preprocessing.
    """
    raw_price = str(raw_price).replace('\xa0', '')
    price_pattern = r'\d+\.?\d*'
    match = re.search(
        price_pattern,
        raw_price.replace(',', '.').replace(' ', '')
    )
    if match:
        return round(float(match.group(0)), 2)
    return float(0)


def del_duplicate(list_images: list):
    print('###', list_images)
    return set(list_images)

class OfferItem(Item):
    url = Field(
        input_processor=MapCompose(strip_string),
        output_processor=TakeFirst()
    )
    article = Field(
        input_processor=MapCompose(strip_string),
        output_processor=TakeFirst()
    )
    price = Field(
        input_processor=MapCompose(filter_price),
        output_processor=TakeFirst()
    )

    old_price = Field(
        input_processor=MapCompose(filter_price),
        output_processor=TakeFirst()
    )

    name = Field(
        input_processor=MapCompose(strip_string),
        output_processor=TakeFirst()
    )

    currency = Field(
        input_processor=MapCompose(strip_string),
        output_processor=TakeFirst()
    )

    description = Field(
        input_processor=MapCompose(strip_string),
        output_processor=TakeFirst()
    )

    images = Field(
        input_processor=Identity(),
        output_processor=Identity()
    )

    is_active = Field(
        input_processor=MapCompose(bool),
        output_processor=TakeFirst()
    )