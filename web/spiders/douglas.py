import scrapy
from web.items import OfferItem
from scrapy.loader import ItemLoader
from html_text import extract_text


class DouglasSpider(scrapy.Spider):
    name = 'douglas'
    allowed_domains = ['douglas.lv']
    start_urls = ['https://www.douglas.lv/lv/katalogs/']

    custom_settings = {
        'CONCURRENT_REQUESTS': 32,
        'USER_AGENT': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 YaBrowser/19.12.0.769 Yowser/2.5 Safari/537.36',
        'ROBOTSTXT_OBEY': False,
        'COOKIES_ENABLED': False,
        'LOG_LEVEL': 'INFO',
        'LOGSTATS_INTERVAL': 3.0,
    }

    def parse(self, response):
        product_urls = response.xpath(
            '//div[@id="products_listing"]//div[contains(@class, "product_element")]/a/@href'
        ).getall()

        for product_url in product_urls:
            yield response.follow(product_url, self.parse_products)

        next_page = response.xpath('//a[@class="page next"]/@href').get()
        if next_page:
            yield response.follow(next_page, self.parse)


    def parse_products(self, response):

        items = response.xpath(
            '//div[@class="items"]//div[contains(@class, "item")]'
        )
        description = extract_text(
            response.xpath('//div[@class="short_description"]').get()
        )
        name = response.xpath('//h1/text()').extract_first()
        photo_images = response.xpath(
            '//div[@class="photos_main"]//a[contains(@id, "photo_main")]/@href').getall()

        for item in items:

            loader = ItemLoader(item=OfferItem(), selector=response)
            loader.add_value('url', response.url)

            article = item.xpath('.//input/@value').extract_first()
            loader.add_value('article', article)

            sub_name = item.xpath('.//span[@class="name"]/text()').extract_first()
            loader.add_value(
                'name', extract_text(name + '' + sub_name)
            )
            sub_images = response.xpath(
                f'//div[@class="photos_main"]//a[contains(@id, "item_photo_{article}")]/@href'
            ).getall()
            images = set(photo_images + sub_images)
            images = list(images)
            loader.add_value('images', images)

            price = item.xpath('./div[@class="price"]//span').get()
            loader.add_value('price', price if price else 0)

            old_price = item.xpath('./div[@class="price"]/span[@class="old_price"]').get()
            loader.add_value('old_price', old_price if old_price else None)

            loader.add_value('is_active', price if price else False)
            loader.add_value('description', description if description else None)
            loader.add_value('currency', 'EUR')
            yield loader.load_item()
