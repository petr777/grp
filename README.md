# GRP


#### Вирутальное окружение

Вирутальное окружение разворачивается через [Poetry](https://python-poetry.org/docs/ "Документация Poetry"):

---
```bash
poetry install  # установка зависимостей
poetry shell    # активация вирутального окружения
```


#### Создание пауков
---

1. Стягиваем свежие изменения и создаем новую ветку.

	```bash
	git pull origin master
	git checkout -b <spider_name>
	```

2. Создаем модуль паука в директории `web/spiders/`, название файла должно совпадать с именем паука (class attribute `name`).

	```bash
	git add web/spiders/<spider_name>.py
	git commit -m "[<spider_name>] add spider"
	git push origin <spider_name>
	```

3. Паук готов. Добавляем файлы в коммит и пушим в репу:

	```bash
	git add scrapper/spiders/<spider_name>.py
	git add redneck_jobs/<spider_name>.py
	git commit -m "[<spider_name>] add spider"
	git push origin <spider_name>
	```

4. Создаем MR.


#### Запуск пауков

---
Запустиь можно например так

```bash
scrapy crawl <spider_name> -o file.csv
```







